get '/' do
  init_contentful
  @recipes = get_all_recipes
  slim :index
end

get '/recipe/:id' do
  init_contentful
  @recipe = get_recipe(params[:id])
  slim :recipe
end