require 'sinatra'
require 'slim'
require './lib/config'
require './lib/contentful_fetcher'
require 'sinatra/static_assets'
require 'kramdown'
require './routes'
require './helpers'


def init_contentful
  @contentful = ContentfulFetcher.new
end

def get_all_recipes
  @contentful.entries
end

def get_recipe(id)
  @contentful.entry(id)
end
