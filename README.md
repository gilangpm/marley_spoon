# Marley Spoon Web Challenge

## Description
This app is built using Sinatra

## Setup
- clone this code
- install gems using bundle
- run the app using this following command `ruby app.rb`

## Accessing the app
- open browser
- put this URL on the address bar http://localhost:4567 