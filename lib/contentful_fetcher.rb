require 'contentful'
require_relative 'config'
  
class ContentfulFetcher

  def initialize
    @client = Contentful::Client.new(
      space: Config.contentful_space_id,
      access_token: Config.contentful_access_token,
      environment: Config.contentful_environment,
      dynamic_entries: :auto
    )
  end

  def entries
    @client.entries(content_type: 'recipe')
  end

  def entry(id)
    @client.entry(id)
  end

end