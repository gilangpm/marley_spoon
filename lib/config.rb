require 'yaml'

class Config
  CONTENTFUL_CONFIG_FILE = File.expand_path('config/contentful.yml')
  YAML.load_file(CONTENTFUL_CONFIG_FILE)["contentful"].map do |key, value|
    define_singleton_method "contentful_#{key}" do
      value
    end
  end
  
end