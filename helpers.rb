helpers do
  def get_tags(recipe)
    begin
      recipe.tags.map(&:name).each do |tag|
        css = ['yui', 'js'].sample
        return '<a class="post-category post-category-'+css+'" href="#">'+tag+'</a>'
      end
    rescue Contentful::EmptyFieldError
      "-"
    end
  end

  def description_html(recipe)
    Kramdown::Document.new(@recipe.description).to_html
  end

  def get_chef(recipe)
    begin
      recipe.chef.name
    rescue Contentful::EmptyFieldError
      "anonimous chef"
    end
  end
end